const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const router = require('./router');

let app = express();

app.use(cors());
app.use(bodyParser.json());
app.use('/', router);

const port = 8080;
app.listen(port, () => {
  console.log(`Server Starts on localhost:${port}`);
});
